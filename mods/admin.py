from django.contrib import admin

# Register your models here.
from .models import File
from .models import Publication
from .models import Image
from .models import Category, Report

#admin.site.register(Publication)
#admin.site.register(Image)

admin.site.register(File)
admin.site.register(Category)
class ImageInline(admin.StackedInline):
    model = Image
    extra = 0

class FileInline(admin.StackedInline):
    model = File
    extra = 0

class PublicationAdmin(admin.ModelAdmin):
    inlines = [ImageInline, FileInline]

class ReportAdmin(admin.ModelAdmin):
    list_display = ('reason', 'comment', '__str__', 'rep_date', )
    list_filter = ('author', 'publicationfk', 'reason')
    search_fields = ('comment', 'publicationfk', 'rep_date')

admin.site.register(Publication, PublicationAdmin)
admin.site.register(Image)
admin.site.register(Report, ReportAdmin)