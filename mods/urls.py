from django.conf.urls import url
from django.conf.urls import include

from . import views

app_name = 'mods'
urlpatterns = [
    url(r'^$', views.listcategories, name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.PublicationView.as_view(), name='publicationview'),
    #url(r'^(?P<pk>[0-9]+)/$', views.Publication.as_view(), name='publicationview'),
    url(r'^(?P<publication_id>[0-9]+)/file/(?P<pk>[0-9]+)/$', views.FileView.as_view(), name='viewfile'),
    # url(r'^(?P<publication_id>[0-9]+)/file/(?P<file_id>[0-9]+)/download/$', views.GoToDownload, name='GoToDownload'),
    url(r'^file/(?P<file_id>[0-9]+)/$', views.downloader, name='goToDownload'),
    url(r'^(?P<publication_id>[0-9]+)/upload/$', views.upload, name='upload'),
    url(r'^(?P<publication_id>[0-9]+)/report/$', views.ReportView.as_view(), name='report'),

    url(r'^user/(?P<author>.+)/', views.UserPublications.as_view(), name='userpublications'),
    url(r'^modpublications/', views.ModPublications.as_view(), name='modpublications'),

    # zarzadzanie publikacjami:
    url(r'^(?P<pk>[0-9]+)/edit/$', views.PublicationEdit.as_view(), name='publicationedit'),
    url(r'^(?P<pk>[0-9]+)/delete/$', views.PublicationDelete.as_view(), name='publicationdelete'),
    #url(r'^newpublication/', views.newPublication, name='newpublication'),
    url(r'^categoryview/(?P<category_slug>.+)/newpublication/', views.PublicationCreate.as_view(), name='newpublicationwithcategory'),
    url(r'^newpublication/', views.PublicationCreate.as_view(), name='newpublication'),

    url(r'^categoryview/(?P<category_slug>.+)/', views.categoryview, name='categoryview'),
    url(r'^ratings/', include('star_ratings.urls', namespace='ratings', app_name='ratings')),
    url(r'^user/(?P<author>.+)/', views.UserPublications.as_view(), name='userpublications'),
]
