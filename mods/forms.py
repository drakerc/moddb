from django import forms
from .models import File, Image, Publication, Category, Report
from django.forms.models import inlineformset_factory
class FileForm(forms.ModelForm):
	class Meta:
		model = File
		fields = ['title', 'version', 'description', 'physicalfile',]

class ImgForm(forms.ModelForm):
	class Meta:
		model = Image
		fields = ['image',]

class PublicationForm(forms.ModelForm):
	class Meta:
		model = Publication
		fields = ['title', 'description', 'slug', 'category', 'video', ]

class ReportForm(forms.ModelForm):
	class Meta:
		model = Report
		fields = ['reason', 'comment', ]

fileformset = inlineformset_factory(Publication, File, form=FileForm, exclude=[],can_delete=True, extra=3, max_num=10)
imageformset = inlineformset_factory(Publication, Image, form=ImgForm, can_delete=True, exclude=[], extra=3, max_num=10)