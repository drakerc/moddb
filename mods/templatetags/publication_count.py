from django import template
from ..models import Publication
register = template.Library()

@register.simple_tag
def notacceptedcount():
    return Publication.objects.filter(availability = False).count()
