# -*- coding: utf-8 -*-
from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from wsgiref.util import FileWrapper
from django.views import generic
from .models import File, Publication, Image, Category, Report
from django.http import HttpResponseRedirect, HttpResponse, StreamingHttpResponse
from .forms import FileForm, PublicationForm, ImgForm, fileformset, imageformset, ReportForm
import mimetypes, os, tempfile, zipfile
from django.http import Http404
from django.forms.formsets import formset_factory
from django.views.generic.edit import UpdateView, DeleteView, CreateView, FormView
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from os.path import basename
from datetime import datetime, date, time


# Create your views here.

class IndexView(generic.ListView):  # ten widok nie jest wykorzystywany, prawdopodobnie do przerobienia na widok publikacji nieopublikowanych dla moderatorow
    template_name = 'mods/index.html'
    context_object_name = 'publicationlist'

    def get_queryset(self):
        return Publication.objects.order_by('-pub_date')

class UserPublications(generic.ListView):
    template_name = 'mods/userpublications.html'
    context_object_name = 'userpublications'

    def get_queryset(self, **kwargs):
        return Publication.objects.filter(author__username=self.kwargs['author'], availability=True).order_by('-pub_date')

class ModPublications(generic.ListView):
    template_name = 'mods/modpublications.html'
    context_object_name = 'modpublications'
    success_url = reverse_lazy('mods:index')

    def post(self, request):
        accepted = request.POST.getlist('accepted')
        for pub in accepted:
            pubobject = get_object_or_404(Publication, pk=pub)
            pubobject.availability = True
            pubobject.save()
            for file in pubobject.get_files():
                file.availability = True
                file.save()
        return self.form_valid()

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_superuser or self.request.user.groups.filter(name='Moderators').exists():
            return super(ModPublications, self).dispatch(request, *args, **kwargs)
        else:
            raise Http404()

    def form_valid(self):
        messages.success(self.request, 'Pomyślnie zaakceptowales publikacje!')
        return HttpResponseRedirect(self.success_url)

    def get_queryset(self, *args, **kwargs):
        return Publication.objects.filter(availability=False)


class PublicationView(generic.DetailView):
    template_name = 'mods/pubdetail.html'
    model = Publication

    def post(self, request, pk):
        view = multidownload(self.request)
        return view

    def get_object(self, queryset=None):
        obj = super(PublicationView, self).get_object(queryset)
        if obj.author is self.request.user or self.request.user.is_superuser:
            return obj
        if not obj.availability:
            raise Http404()
        else:
            return obj

    def filelist(self):
        return File.objects.filter(publicationfk=self.object, availability=1)

def multidownload(request):
    files = request.POST.getlist('multidownload')
    if len(files) is 1: #tylko 1 plik, przekierowac do gotodownlaod zeby nie zipowac
        fileobj = get_object_or_404(File, pk=files[0])
        return downloader(request, fileobj.id)
    temp = tempfile.NamedTemporaryFile()
    archive = zipfile.ZipFile(temp, 'w', zipfile.ZIP_DEFLATED)
    for file in files:
        fileobj = get_object_or_404(File, pk=file)
        file_full_path = "{}/{}".format(settings.MEDIA_ROOT, fileobj.physicalfile)
        archive.write(file_full_path, basename(file_full_path))
        fileobj.downloads += 1
        fileobj.save()
    archive.close()
    wrapper = FileWrapper(open(temp.name, 'rb'), 4096)
    response = HttpResponse(wrapper, content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=MultiDownload_{0}.zip'.format(datetime.now().strftime("%Y%m%d%H%M"))
    response['Content-Length'] = temp.tell()
    temp.seek(0)
    return response

class FileView(generic.DetailView):
    template_name = 'mods/detail.html'
    model = File

    def get_object(self, queryset=None):
        obj = super(FileView, self).get_object(queryset)
        if not obj.availability:
            raise Http404()
        else:
            return obj

class PublicationCreate(CreateView, LoginRequiredMixin):
    model = Publication
    form_class = PublicationForm
    template_name = 'mods/pubedit.html'
    login_url = '/login/'

    def get_success_url(self):
        self.success_url = reverse('mods:publicationview', args=(self.object.id,))
        return self.success_url

    def get_initial(self):
        initial = super(PublicationCreate, self).get_initial()
        if self.kwargs is not None and 'category_slug' in self.kwargs:
            initial['category'] = Category.objects.get(slug=self.kwargs['category_slug'])
        return initial

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        pubform = self.get_form(form_class)
        fileform = fileformset(instance=self.object)
        imgform = imageformset(instance=self.object)

        return self.render_to_response(
            self.get_context_data(pubform=pubform, fileform=fileform, imgform=imgform))

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        pubform = self.get_form(form_class)
        fileform = fileformset(self.request.POST, self.request.FILES, instance=self.object)
        imgform = imageformset(self.request.POST, self.request.FILES, instance=self.object)

        if pubform.is_valid() and fileform.is_valid() and imgform.is_valid():
            return self.form_valid(pubform, fileform, imgform)
        else:
            return self.form_invalid(pubform, fileform, imgform)

    def form_valid(self, pubform, fileform, imgform):
        pubform.availability = True

        self.object = pubform.save(commit=False)
        self.object.author = self.request.user
        if self.request.user.is_superuser:
            self.object.availability = True
        self.object.save()

        fileform.instance = self.object
        files = fileform.save(commit=False)
        for file in files:
            file.author = self.request.user
            if self.request.user.is_superuser:
                file.availability = True
            file.save()

        imgform.instance = self.object
        imgform.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, pubform, fileform, imgform):
        return self.render_to_response(self.get_context_data(pubform=pubform,
                                                             fileform=fileform,
                                                             imgform=imgform))

class PublicationEdit(UpdateView, LoginRequiredMixin):
    model = Publication
    form_class = PublicationForm
    template_name = 'mods/pubedit.html'
    login_url = '/login/'

    def get_success_url(self):
        self.success_url = reverse('mods:publicationview', args=(self.object.id,))
        return self.success_url

    def get_context_data(self, **kwargs):
        context = super(PublicationEdit, self).get_context_data(**kwargs)
        if self.request.POST:
            context['pubform'] = PublicationForm(self.request.POST, instance=self.object)
            context['fileform'] = fileformset(self.request.POST, self.request.FILES, instance=self.object)
            context['imgform'] = imageformset(self.request.POST, self.request.FILES, instance=self.object)
        else:
            context['pubform'] = PublicationForm(instance=self.object)
            context['fileform'] = fileformset(instance=self.object)
            context['imgform'] = imageformset(instance=self.object)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        pubform = self.get_form(form_class)
        fileform = fileformset(self.request.POST, self.request.FILES, instance=self.object)
        imgform = imageformset(self.request.POST, self.request.FILES, instance=self.object)

        if pubform.is_valid() and fileform.is_valid() and imgform.is_valid():
            return self.form_valid(pubform, fileform, imgform)
        else:
            return self.form_invalid(pubform, fileform, imgform)

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.author is self.request.user or self.request.user.is_superuser:
            return super(PublicationEdit, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(reverse('mods:publicationview', args=(obj.id,)))

    def form_valid(self, pubform, fileform, imgform):
        self.object = pubform.save()
        fileform.instance = self.object
        fileform.save()
        imgform.instance = self.object
        imgform.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, pubform, fileform, imgform):
        return self.render_to_response(self.get_context_data(pubform=pubform,
                                                             fileform=fileform,
                                                             imgform=imgform))


class PublicationDelete(DeleteView, LoginRequiredMixin):
    model = Publication
    form_class = PublicationForm
    template_name = 'mods/publicationdelete.html'
    login_url = '/login/'
    success_url = reverse_lazy('mods:categories')

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.author is self.request.user or self.request.user.is_superuser:
            return super(PublicationDelete, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(reverse('mods:publicationview', args=(obj.id,)))


class ReportView(SuccessMessageMixin, FormView, LoginRequiredMixin):
    form_class = ReportForm
    model = Report
    login_url = '/login/'
    template_name = 'mods/publicationreport.html'
    success_url = reverse_lazy('mods:categories')

    def form_valid(self, form):
        publication = Publication.objects.get(id=self.kwargs['publication_id'])
        obj = form.save(commit=False)
        obj.publicationfk = publication
        obj.author = self.request.user
        form.save()
        messages.success(self.request, 'Pomyślnie zgłosiłeś publikację. Dziękujemy.!')
        return HttpResponseRedirect(reverse('mods:publicationview', args=(publication.id,)))


# to do wyswietlania samych kategorii, ktore nie posiadaja podkategorii
def listcategories(request):
    categorylist = Category.objects.filter(subcategoryof=None).order_by('title')
    context = {'categorylist': categorylist}
    return render(request, 'mods/listcategories.html', context)


# widok wybranej kategorii wraz z jej podkategoriami i publikacjami
def categoryview(request, category_slug):
    category = Category.objects.get(slug=category_slug)
    publications = Publication.objects.filter(category=category, availability=1)
    subcategories = Category.objects.filter(subcategoryof=category)
    return render(request, 'mods/catview.html',
                  {'publications': publications, 'category': category, 'subcategories': subcategories, })


def downloader(request, file_id):
    file = get_object_or_404(File, pk=file_id)
    file_full_path = "{}/{}".format(settings.MEDIA_ROOT, file.physicalfile)
    response = StreamingHttpResponse(FileWrapper(open(file_full_path, 'rb'), 4096),
                                     content_type=mimetypes.guess_type(file_full_path)[0])
    response['Content-Disposition'] = "attachment; filename={0}".format(file.physicalfile)
    response['Content-Length'] = os.path.getsize(file_full_path)
    file.downloads += 1
    file.save()
    return response


@login_required(login_url='account_login')
def upload(request, publication_id):
    form = FileForm(request.POST, request.FILES)
    publication = get_object_or_404(Publication, pk=publication_id)
    if request.method == 'POST':
        if form.is_valid():
            newfile = File(physicalfile=request.FILES['physicalfile'],
                           title=request.POST['title'],
                           version=request.POST['version'],
                           description=request.POST['description'],
                           publicationfk=publication
                           )
            newfile.save()
            if request.user.is_staff:
                author = request.user
            return HttpResponseRedirect(reverse('mods:viewfile', args=(publication.id, newfile.id,)))

    else:
        form = FileForm()
    return render(request, 'mods/upload.html', {
        'form': form,
    })





@login_required(login_url='account_login')
def newPublication(request):
    fileformset = formset_factory(FileForm, extra=2)
    if request.method == 'POST':
        pubform = PublicationForm(request.POST)
        # fileform = FileForm(request.POST, request.FILES)
        imgform = ImgForm(request.POST, request.FILES)
        formset = fileformset(request.POST, request.FILES)
        if pubform.is_valid() and formset.is_valid() and imgform.is_valid():
            newpublication = Publication(
                title=request.POST['title'],
                description=request.POST['description'],
                slug=request.POST['slug'],
                availability=True,
                author=request.user
            )
            newpublication.save()

            for form in formset.cleaned_data:
                newfile = File(
                    physicalfile=form.get('physicalfile'),
                    title=form.get('title'),
                    version=form.get('version'),
                    description=form.get('description'),
                    publicationfk=newpublication,
                    availability=True
                )
                newfile.save()

                newimg = Image(image=request.FILES['image'],
                               publication=newpublication
                               )
                newimg.save()

        return HttpResponseRedirect(reverse('mods:publicationview', args=(newpublication.id,)))
    else:
        pubform = PublicationForm()
        fileform = FileForm()
        imgform = ImgForm()
        formset = fileformset()
    return render(request, 'mods/newpublication.html', {
        'pubform': pubform,
        'fileform': fileform,
        'imgform': imgform,
        'formset': formset,
    })

class UserPublications(generic.ListView):
    template_name = 'mods/userpublications.html'
    context_object_name = 'userpublications'

    def get_queryset(self, **kwargs):
        return Publication.objects.filter(author__username=self.kwargs['author'], availability=True).order_by('-pub_date')
