# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from embed_video.fields import EmbedVideoField
from versatileimagefield.fields import VersatileImageField
import uuid, os
# Create your models here.


def validate_extension(value):
	ext = os.path.splitext(value.name)[-1]
	valid = ['.zip', 'rar', 'exe', 'tar.gz', '.7z']
	if not ext in valid:
		raise ValidationError(u'Zly format pliku, akceptowane jest tylko zip, rar, exe, tar.gz i 7z')


class Category(models.Model):
	title = models.CharField('Tytul', max_length=100)
	image = VersatileImageField('Wrzuc obrazek kategorii', default='default.gif')
	description = models.CharField('Opis', max_length=450)
	subcategoryof = models.ForeignKey('self', null=True, blank=True, related_name = 'podkategoria')
	slug = models.SlugField(max_length=40, unique=True)

	def __str__(self):
		return self.title

	def get_publication_count(self):
		return self.publication.count()

	def get_subcategories_count(self):
		return self.podkategoria.all().count()

	def get_subpublications_count(self):
		sum = 0
		for sub in self.podkategoria.all():
			sum += Publication.objects.filter(category=sub).count()
		return sum

	def get_subcats(self):
		return self.podkategoria.all()

	#mptt
	def get_sumofpublications_count(self):
		sum = 0
		sum += self.publication.count()
		for sub in self.podkategoria.all():
			sum += Publication.objects.filter(category=sub).count()
		return sum

	class Meta:
		unique_together = ('slug', 'subcategoryof',)

class Publication(models.Model):
	title = models.CharField('Tytul', max_length=100)
	author = models.ForeignKey(User, verbose_name=('Author'), blank=True, default=0)
	pub_date = models.DateTimeField('Data publikacji', default=datetime.now)
	mod_date = models.DateTimeField('Data ostatniej modyfikacji', auto_now=True)
	description = models.CharField('Opis', max_length=450)
	slug = models.SlugField(max_length=40, unique=True)
	category = models.ForeignKey(Category, verbose_name=('Kategoria'), default=0, related_name='publication')
	availability = models.BooleanField('Publikacja widoczna publicznie czy prywatny (zaakceptowany lub nie)', default=0)
	video = EmbedVideoField(blank=True)

	def __str__(self):
		return self.title

	def TotalDownloads(self):
		return self.files.aggregate(total=models.Sum('downloads'))['total']

	def get_images(self):
		return self.images.all()

	def get_first_image(self):
		return self.images.first()

	def get_files(self):
		return self.files.all()

	class Meta:
		get_latest_by = 'pub_date'

def randomfilenames(instance, filename):
	name = filename.split('.')[0]
	extension = filename.split('.')[-1]
	newname = "%s_%s.%s" % (uuid.uuid4(), name, extension)
	return os.path.join(newname)

class File(models.Model):
	title = models.CharField('Tytul', max_length=100)
	version = models.CharField(verbose_name=u"Wersja", max_length=100)
	author = models.ForeignKey(User, verbose_name=('Author'), blank=True, default=0)
	pub_date = models.DateTimeField('Data publikacji', default=datetime.now)
	mod_date = models.DateTimeField('Data ostatniej modyfikacji', auto_now=True)
	description = models.CharField('Opis', max_length=450)
	downloads = models.IntegerField(default=0)
	physicalfile = models.FileField('Wrzuc plik', upload_to=randomfilenames, validators=[validate_extension])
	publicationfk = models.ForeignKey(Publication,on_delete=models.CASCADE, related_name='files')
	availability = models.BooleanField('Plik widoczny publicznie (zaakceptowana) ?', default=0)

	def __str__(self):
		return self.title

	def get_file_size(self):
		print self.physicalfile.size

class Image(models.Model):
	publication = models.ForeignKey(Publication, related_name='images')
	image = VersatileImageField('Wrzuc obrazek')

class Report(models.Model):
	REASONS = (
		('OB', 'Tresci obrazliwe'),
		('ZK', 'Zła kategoria'),
		('ZO', 'Zły opis'),
		('WI', 'Wirus'),
	)

	publicationfk = models.ForeignKey(Publication,on_delete=models.CASCADE, related_name='reports')
	author = models.ForeignKey(User, verbose_name=('Author'), blank=True, default=0, related_name='reportauthor')
	moderator = models.ForeignKey(User, verbose_name=('Moderator'), blank=True, default=0)
	rep_date = models.DateTimeField('Data zgłoszenia', default=datetime.now)
	reason = models.CharField(max_length=2, choices=REASONS)
	comment = models.CharField('Komentarz', max_length=250, null=True, blank=True)

	def __str__(self):
		return Publication.objects.get(id=self.publicationfk.id).title