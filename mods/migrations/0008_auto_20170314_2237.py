# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-14 22:37
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mods', '0007_auto_20170314_2120'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='file',
            name='publication',
        ),
        migrations.AddField(
            model_name='publication',
            name='pubfiles',
            field=models.ManyToManyField(to='mods.File'),
        ),
        migrations.AlterField(
            model_name='file',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 3, 14, 22, 37, 3, 930305), verbose_name='Data publikacji'),
        ),
        migrations.AlterField(
            model_name='publication',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 3, 14, 22, 37, 3, 927312), verbose_name='Data publikacji'),
        ),
    ]
