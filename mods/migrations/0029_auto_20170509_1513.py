# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-05-09 15:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mods', '0028_auto_20170427_1437'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='mod_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Data ostatniej modyfikacji'),
        ),
        migrations.AlterField(
            model_name='publication',
            name='mod_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Data ostatniej modyfikacji'),
        ),
    ]
