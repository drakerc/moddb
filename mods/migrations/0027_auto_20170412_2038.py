# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-12 18:38
from __future__ import unicode_literals

from django.db import migrations
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mods', '0026_publication_video'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='image',
            field=versatileimagefield.fields.VersatileImageField(default='default.gif', upload_to=b'', verbose_name='Wrzuc obrazek kategorii'),
        ),
        migrations.AlterField(
            model_name='image',
            name='image',
            field=versatileimagefield.fields.VersatileImageField(upload_to=b'', verbose_name='Wrzuc obrazek'),
        ),
    ]
